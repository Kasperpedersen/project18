# Project18 
MAKE SURE TO DOWNLOAD THE FOLDER AS A .ZIP !



To help organise our results for the UNIBO exam in Machine Learning

## Files:
There are two main files:
Master.ipynb and
Aux.ipynb

The Master file is where all code, tables, and desriptions are. The master file does, however, not work by it self. That is why, one needs the AUX file which is run in the very top of the Master. The AUX file is dependent on three other files: AEEEM_DATA, NASA_DATA, and PROMISE_DATA. All of which should be saved in the same folder so it can run automatically. 


Functions that we have created ourselves can be seen in the AUX file. They have been moved there to create more space for the model.
When models are run in the Master file, one may run into a warning that a slice of a panda dataframe is being overwritten. This is not a major issues as everything still works. 
